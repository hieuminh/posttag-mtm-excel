﻿using PostTag.Core.Models;
using System.Threading.Tasks;

namespace PostTag.Core.Repository
{
    public interface IPostRepository : IGenericRepository<Post>
    {
        Task<bool> ExistAsync(string id);
    }
}
