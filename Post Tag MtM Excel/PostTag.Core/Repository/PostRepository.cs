﻿using Microsoft.EntityFrameworkCore;
using PostTag.Core.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PostTag.Core.Repository
{
    public class PostRepository : GenericRepository<Post>, IPostRepository
    {
        public PostRepository(ToDoContext context) : base(context)
        {
        }

        public bool Exist(string id)
        {
            return _context.Posts.Any(t => t.Id == id);
        }

        public async Task<bool> ExistAsync(string id)
        {
            return await _context.Posts.AnyAsync(t => t.Id == id);
        }
    }
}
