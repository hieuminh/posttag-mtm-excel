﻿using PostTag.Core.Models;
using System.Threading.Tasks;

namespace PostTag.Core.Repository
{
    public interface ITagRepository : IGenericRepository<Tag>
    {
        Task<bool> ExistAsync(string id);
    }
}
