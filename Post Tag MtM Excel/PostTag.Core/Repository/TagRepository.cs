﻿using Microsoft.EntityFrameworkCore;
using PostTag.Core.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PostTag.Core.Repository
{
    public class TagRepository : GenericRepository<Tag>, ITagRepository
    {
        public TagRepository(ToDoContext context) : base(context)
        {
        }

        public bool Exist(string id)
        {
            return _context.Tags.Any(t => t.Id == id);
        }

        public async Task<bool> ExistAsync(string id)
        {
            return await _context.Tags.AnyAsync(t => t.Id == id);
        }
    }
}
