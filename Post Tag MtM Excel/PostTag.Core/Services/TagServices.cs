﻿using PostTag.Core.Models;
using PostTag.Core.Repository;
using System.Threading.Tasks;

namespace PostTag.Core.Services
{
    public class TagServices : BaseService<Tag>, ITagServices
    {
        private readonly ITagRepository _tagRepository;

        public TagServices(IGenericRepository<Tag> repository, ITagRepository tagRepository) : base(repository)
        {
            _tagRepository = tagRepository;
        }

        public async Task<bool> ExistAsync(string id)
        {
            return await _tagRepository.ExistAsync(id);
        }
    }
}
