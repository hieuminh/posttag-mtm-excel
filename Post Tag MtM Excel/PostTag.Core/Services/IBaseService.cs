﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PostTag.Core.Services
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        int Add(TEntity entity);

        Task<int> AddAsync(TEntity entity);

        bool Update(TEntity entity);

        Task<bool> UpdateAsync(TEntity entity);

        bool Delete(string id);

        Task<bool> DeleteAsync(string id);

        TEntity GetById(string id);

        Task<TEntity> GetByIdAsync(string id);

        //Task<Paginated<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
        //    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        //    string includeProperties = "", int pageIndex = 1, int pageSize = 10);

        Task<IEnumerable<TEntity>> GetAllAsync();
    }
}
