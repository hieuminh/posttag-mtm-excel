﻿using PostTag.Core.Models;
using System.Threading.Tasks;

namespace PostTag.Core.Services
{
    public interface IPostServices : IBaseService<Post>
    {
        Task<bool> ExistAsync(string id);
    }
}
