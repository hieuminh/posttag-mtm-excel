﻿using PostTag.Core.Models;
using PostTag.Core.Repository;
using System.Threading.Tasks;

namespace PostTag.Core.Services
{
    public class PostServices : BaseService<Post>, IPostServices
    {
        private readonly IPostRepository _postRepository;

        public PostServices(IGenericRepository<Post> repository, IPostRepository postRepository) : base(repository)
        {
            _postRepository = postRepository;
        }

        public async Task<bool> ExistAsync(string id)
        {
            return await _postRepository.ExistAsync(id);
        }
    }
}
