﻿using PostTag.Core.Models;
using System.Threading.Tasks;

namespace PostTag.Core.Services
{
    public interface ITagServices : IBaseService<Tag>
    {
        Task<bool> ExistAsync(string id);

    }
}
