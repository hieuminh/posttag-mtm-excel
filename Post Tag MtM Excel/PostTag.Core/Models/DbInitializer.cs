﻿using System;
using System.Linq;

namespace PostTag.Core.Models
{
    public class DbInitializer
    {
        public static void Seed(ToDoContext context)
        {
            context.Database.EnsureCreated();
            if (context.Posts.Any())
            {
                return;
            }
            var posts = new Post[]
            {
                new Post()
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "ASP .NET Content",
                    Slug = "ASP .NET Content",
                    Content="ASP.NET supports industry standard authentication protocols. Built-in features help protect your apps against cross-site scripting (XSS) and cross-site request forgery (CSRF).",
                    Views=20,
                    Published=true,
                    PublishedDate = DateTime.Now,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now

                },
                 new Post()
                {
                     Id = Guid.NewGuid().ToString(),
                     Title = "Java Content",
                     Slug = "Java Content",
                     Content="Java technology allows you to work and play in a secure computing environment. Upgrading to the latest Java version improves the security of your system, as older versions do not include the latest security updates.",
                     Views=10,
                     Published=true,
                     PublishedDate = DateTime.Now,
                     CreatedDate = DateTime.Now,
                     ModifiedDate = DateTime.Now

                },
                  new Post()
                {
                     Id = Guid.NewGuid().ToString(),
                     Title = "Python Content",
                     Slug = "Python Content",
                     Content="The mission of the Python Software Foundation is to promote, protect, and advance the Python programming language, and to support and facilitate the growth of a diverse and international community of Python",
                     Views=40,
                     Published=true,
                     PublishedDate = DateTime.Now,
                     CreatedDate = DateTime.Now,
                     ModifiedDate = DateTime.Now

                }
            };
            context.Posts.AddRange(posts);
            var tags = new Tag[]
            {
                new Tag()
                {
                     Id = Guid.NewGuid().ToString(),
                        Name = "C# Class",
                        Slug = "C# Class",
                        Content = "About C# Class",

                },
                new Tag()
                {
                    Id = Guid.NewGuid().ToString(),
                        Name = "Java Class",
                        Slug = "Java Class",
                        Content = "About Java Class",

                },
                new Tag()
                {
                    Id = Guid.NewGuid().ToString(),
                        Name = "Python Class",
                        Slug = "Python Class",
                        Content = "About Python Class",

                }
            };
            context.Tags.AddRange(tags);
            var tagposts = new PostTag[]
            {
                new PostTag()
                {
                    Post= posts.Single(p=>p.Title=="ASP .NET Content"),
                    Tag = tags.Single(x=>x.Name=="C# Class")
                },
                new PostTag()
                {
                    Post=posts.Single(p=>p.Title=="Java Content"),
                    Tag = tags.Single(x=>x.Name=="Java Class")
                },
                new PostTag()
                {
                    Post=posts.Single(p=>p.Title=="Python Content"),
                    Tag = tags.Single(x=>x.Name=="Python Class")
                }
            };
            context.PostTags.AddRange(tagposts);
            context.SaveChanges();
        }
    }
}
