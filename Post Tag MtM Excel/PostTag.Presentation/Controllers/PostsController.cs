﻿using ClosedXML.Excel;
using ExcelDataReader;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using PostTag.Core.Models;
using PostTag.Core.Services;
using PostTag.Presentation.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PostTag.Presentation.Controllers
{
    public class PostsController : Controller
    {

        private readonly IPostServices _postServices;
        private readonly ITagServices _tagServices;
        private readonly ToDoContext _context;

        public PostsController(IPostServices postServices, ITagServices tagServices, ToDoContext context)
        {

            _postServices = postServices;
            _tagServices = tagServices;
            _context = context;
        }

        // GET: Posts
        public async Task<IActionResult> Index()
        {

            var result =  _context.Posts.Include(x => x.PostTags).ThenInclude(x => x.Tag);

            return View(result);
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _postServices.GetByIdAsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Posts/Create
        public async Task<IActionResult> Create()
        {

            ViewData["TagId"] = new SelectList( await _tagServices.GetAllAsync(), "Id", "Name");

            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClassPostViewModel classPostViewModel)
        {

            if (ModelState.IsValid)
            {
                var post = new Post();

                post.Id = Guid.NewGuid().ToString();
                post.Content = classPostViewModel.Content;
                post.CreatedDate = classPostViewModel.CreatedDate;
                post.ModifiedDate = classPostViewModel.ModifiedDate;
                post.Published = classPostViewModel.Published;
                post.PublishedDate = classPostViewModel.PublishedDate;
                post.Slug = classPostViewModel.Slug;
                post.Title = classPostViewModel.Title;
                post.Views = classPostViewModel.Views;


                var postTags = new List<PostTag.Core.Models.PostTag>();

                foreach (var item in classPostViewModel.tags)
                {
                    var postTag = new PostTag.Core.Models.PostTag()
                    {
                        PostId = post.Id,
                        TagId = item,


                    };
                    postTags.Add(postTag);

                }
                post.PostTags = postTags;

                _postServices.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }

            return View(classPostViewModel);    
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _postServices.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Title,Slug,Content,Views,Published,PublishedDate,CreatedDate,ModifiedDate")] Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _postServices.Update(post);

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _postServices.ExistAsync(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _postServices.GetByIdAsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {

            var post = await _postServices.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ImportClassFromExcel()
        {
            return View();
        }



        [HttpPost]
        [Obsolete]
        public IActionResult ImportClassFromExcel(IFormFile file, [FromServices] IHostingEnvironment hostingEnvironment)
        {
            var filePath = $"{hostingEnvironment.WebRootPath}\\files\\{file.FileName}";
            var fileLocation = new FileInfo(filePath);
            var listPost = new List<Post>();
            using (FileStream package = System.IO.File.Create(filePath))
            {

                file.CopyTo(package);
                package.Flush();
            }

            var posts = this.GetPostList(filePath);
            return View(posts);
 
         }

        private object GetPostList(string filePath)
        {
            var listPost = new List<Post>();
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    int count = 0;
                    while (reader.Read())
                    {
                        if (count > 1)
                        {
                            listPost.Add(new Post()
                            {
                                Title = reader.GetValue(0).ToString(),
                                Slug = reader.GetValue(1).ToString(),
                                Content = reader.GetValue(2).ToString(),
                                Views = Convert.ToInt32(reader.GetValue(3).ToString()),
                                Published = Convert.ToBoolean(reader.GetValue(4).ToString()),
                                PublishedDate = Convert.ToDateTime(reader.GetValue(5).ToString()),
                                CreatedDate = Convert.ToDateTime(reader.GetValue(6).ToString()),
                                ModifiedDate = Convert.ToDateTime(reader.GetValue(7).ToString())


                            });
                        }
                        count++;
                    }
                }

            }


            return listPost;
        }
    

        public IActionResult ExportClassToExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Post");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Title";
                worksheet.Cell(currentRow, 2).Value = "Slug";
                worksheet.Cell(currentRow, 3).Value = "Content";
                worksheet.Cell(currentRow, 4).Value = "Views";
                worksheet.Cell(currentRow, 5).Value = "Published";
                worksheet.Cell(currentRow, 6).Value = "PublishedDate";
                worksheet.Cell(currentRow, 7).Value = "CreatedDate";
                worksheet.Cell(currentRow, 8).Value = "ModifiedDate";

                foreach (var item in _context.Posts.ToList())
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.Title;
                    worksheet.Cell(currentRow, 2).Value = item.Slug;
                    worksheet.Cell(currentRow, 3).Value = item.Content;
                    worksheet.Cell(currentRow, 4).Value = item.Views;
                    worksheet.Cell(currentRow, 5).Value = item.Published;
                    worksheet.Cell(currentRow, 6).Value = item.PublishedDate;
                    worksheet.Cell(currentRow, 7).Value = item.CreatedDate;
                    worksheet.Cell(currentRow, 8).Value = item.ModifiedDate;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "Post.xlsx");
                }
            }
        }
    }
}
